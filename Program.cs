﻿using Raylib_cs;
using static Raylib_cs.Raylib;

public enum Tipo
{
    I,
    O,
    T,
    J,
    L,
    S,
    Z
}

public struct Tetramino
{
    public int Coluna{get;set;}
    public Tipo Tipo{get;set;}
}

public class Program
{
    private const int LARGURA_MATRIZ = 10;
    private const int ALTURA_MATRIZ = 24;
    private const int CELULA_LARGURA = 25;
    private const int CELULA_ALTURA = 25;
    private const char VAZIO = ' ';
    private static char[,] matriz;

    static Program()
    {
        matriz = new char[ALTURA_MATRIZ,LARGURA_MATRIZ];
        for (int i = 0; i < ALTURA_MATRIZ; i++)
            for(int j = 0; j < LARGURA_MATRIZ; j++)
                matriz[i,j] = VAZIO;
    }
    
    static void GerarTetramino()
    {
        
    } 

    static void DesenhaMatriz(int x, int y)
    {
        for (int linha = 0; linha < ALTURA_MATRIZ; linha++)
            for(int coluna = 0; coluna < LARGURA_MATRIZ; coluna++)
                DrawRectangleLines(x + CELULA_LARGURA * coluna, y + CELULA_ALTURA * linha, CELULA_LARGURA, CELULA_ALTURA, Color.RED);
    }
    public static void Main()
    {
        InitWindow(480, 640, "Tetris");

        while(!WindowShouldClose())
        {
            //lógica

            //desenho
            BeginDrawing();
            ClearBackground(Color.GRAY);
            DesenhaMatriz(10, 10);
            EndDrawing();
        }

        CloseWindow();
    }
}
